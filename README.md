#angular-day-schedule-selector

This is an Angular 1.x directive that wraps the jQuery-based day-schedule-selector.

##Usage

```<div day-schedule days="[0,1,2,3,4,5,6]" interval="60" startTime="00:00" endTime="24:00" onSelectStart="functionName" onSelectEnd="functionName" onDataChange="functionName"></div>```