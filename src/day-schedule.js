require('day-schedule-selector/src');
angular.module('day-schedule', []).directive('daySchedule', [function() {
    return {
        restrict: 'A',
        scope: {
            model: '=',
            days: '=',
            interval: '@',
            startTime: '@',
            endTime: '@',
            enabled: '=',
            onSelectStart: '&',
            onSelectEnd: '&',
            onDataChange: '&'
        },
        link: function (scope, elem, attrs) {
            var newAttrs = jQuery.extend({}, attrs);
            if (scope.days) {
                newAttrs.days = scope.days;
            }
            if (scope.onSelectStart) {
                newAttrs.onSelectStart = scope.onSelectStart;
            }
            if (scope.onSelectEnd) {
                newAttrs.onSelectEnd = scope.onSelectEnd;
            }

            newAttrs.onDataChange = function(newData) {
                scope.$apply(function() {
                    scope.model = newData;
                });
                if (scope.onDataChange) {
                    scope.onDataChange(newData);
                }
            }

            $(elem).dayScheduleSelector(newAttrs);

            if (scope.model) {
                $(elem).data('artsy.dayScheduleSelector').deserialize(scope.model);
            }

            scope.$watch('model', function(newVal, oldVal) {
                if (newVal !== oldVal) {
                    $(elem).data('artsy.dayScheduleSelector').clear();
                    $(elem).data('artsy.dayScheduleSelector').deserialize(newVal);
                }
            }, true);

            scope.$watch('enabled', function(newVal, oldVal) {
                if (newVal === true) {
                    $(elem).data('artsy.dayScheduleSelector').enable();
                } else if (newVal === false) {
                    $(elem).data('artsy.dayScheduleSelector').disable();
                }
            });
        }
    }
}]);
